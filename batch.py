import os
import shutil
import sys
from PIL import Image

source = '%s:/Users/%s/AppData/Local/Packages/Microsoft.Windows.ContentDeliveryManager_cw5n1h2txyewy/LocalState/' \
         'Assets/' % (sys.argv[1], sys.argv[2])
destination = sys.argv[3]
for file in os.listdir(source):
    try:
        width, height = Image.open(source + file).size
    except OSError:
        print(source + file)
        continue
    if height >= 1080 and width >= 1920:
        shutil.copyfile(source + file, destination + file + '.jpg')
