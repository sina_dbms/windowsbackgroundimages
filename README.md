## Foreword

Tired of boring Background images on your windows?!  
This small python script copies the lock screen images to a destination, which you specify.  

## Installation
  
In order to run this script you need to have python installed (the version doesn't matter). After that make sure that you have the _PIL_ library installed. Install this library with the following command in your CMD or Powershell:  

`pip install PIL`

## Run

Run the script using the following command:

`python batch.py <drive_letter> <user> <destination>`

* Replace the `<drive_letter>` with the installation location of your windows.
* Replace the `<user>` with your user name in the windows. For that go in the `C:\Users\` directory in your windows explorer and find the right folder name which corresponds your user name.
* Replace the `<destination>` with the target folder, where you want to copy the images. Destination should be followed by a trailing slash. If the destination contains spaces make sure to wrap that in double quotations. 

An example of the above command would be:
  
`python batch.py C Sina "D:/Media/Photos/Windows Images/"`

**HAVE FUN!**